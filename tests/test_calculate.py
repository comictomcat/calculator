import pytest
from fastapi.testclient import TestClient

from app.main import app

client = TestClient(app)


@pytest.mark.parametrize(
    "operand1, operand2, operator, expected_result",
    [
        (1, 2, "+", 3),
        (5, 3, "-", 2),
        (2, 4, "*", 8),
        (6, 2, "/", 3),
    ],
)
def test_calculate(operand1, operand2, operator, expected_result):
    response = client.post(
        "/api/v1/calculate",
        json={
            "operand1": operand1,
            "operand2": operand2,
            "operator": operator,
        },
        headers={"Content-Type": "application/json"},
    )
    assert response.status_code == 200
    assert response.json()["result"] == expected_result


def test_calculate_division_by_zero():
    response = client.post("/api/v1/calculate", json={"operand1": 10, "operand2": 0, "operator": "/"})
    assert response.status_code == 400


def test_calculate_wrong_operator():
    response = client.post("/api/v1/calculate", json={"operand1": 10, "operand2": 0, "operator": "@"})
    assert response.status_code == 400


def test_calculate_wrong_operand():
    response = client.post("/api/v1/calculate", json={"operand1": "test", "operand2": 0, "operator": "+"})
    assert response.status_code == 422
