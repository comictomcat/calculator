document.getElementById('calculator-form').addEventListener('submit', async function(event) {
    event.preventDefault();

    const operand1 = parseFloat(document.getElementById('operand1').value);
    const operand2 = parseFloat(document.getElementById('operand2').value);
    const operator = document.getElementById('operator').value;

    const response = await fetch('/api/v1/calculate', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ operand1, operand2, operator })
    });

    const result = await response.json();
    document.getElementById('result').innerText = `Result: ${result.result}`;
});
