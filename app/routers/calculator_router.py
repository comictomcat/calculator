from fastapi import APIRouter, Depends, HTTPException

from app.dependencies import get_calculator_service
from app.schemas.operation_schema import OperationRequest, OperationResponse
from app.services.calculator_service import CalculatorService

router = APIRouter()


@router.post(
    "/calculate",
    response_model=OperationResponse,
    summary="Perform arithmetic calculation",
    description="Endpoint to perform arithmetic calculations. Accepts JSON payload with operand1, operand2, and operator, and returns the result.",
)
def calculate(
    operation: OperationRequest,
    service: CalculatorService = Depends(get_calculator_service),
) -> OperationResponse:
    """
    Perform arithmetic calculation.

    This endpoint performs arithmetic calculations based on the provided operands and operator.
    It accepts a JSON payload with operand1, operand2, and operator, and returns the result.
    """
    try:
        result = service.calculate(
            operation.operand1,
            operation.operand2,
            operation.operator,
        )
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))
    return OperationResponse(result=result)
