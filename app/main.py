from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from app.routers import calculator_router

app = FastAPI()

app.include_router(calculator_router.router, prefix="/api/v1")

# Serve static files
app.mount("/", StaticFiles(directory="app/static", html=True), name="static")


@app.get("/")
def read_root():
    return {"message": "Calculator API"}
