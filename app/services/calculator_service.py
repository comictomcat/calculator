from app.services.strategies.addition_strategy import AdditionStrategy
from app.services.strategies.base_strategy import OperationStrategy
from app.services.strategies.division_strategy import DivisionStrategy
from app.services.strategies.multiplication_strategy import MultiplicationStrategy
from app.services.strategies.subtraction_strategy import SubtractionStrategy


class CalculatorService:
    def __init__(self) -> None:
        self.strategies = {
            "+": AdditionStrategy(),
            "-": SubtractionStrategy(),
            "*": MultiplicationStrategy(),
            "/": DivisionStrategy(),
        }

    def calculate(self, operand1: float, operand2: float, operator: str) -> float:
        strategy: None | OperationStrategy = self.strategies.get(operator)
        if not strategy:
            raise ValueError(f"Unsupported operation: {operator}")
        return strategy.execute(operand1, operand2)
