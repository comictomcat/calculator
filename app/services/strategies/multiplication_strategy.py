from app.services.strategies.base_strategy import OperationStrategy


class MultiplicationStrategy(OperationStrategy):
    def execute(self, operand1: float, operand2: float) -> float:
        return operand1 * operand2
