from app.services.strategies.base_strategy import OperationStrategy


class DivisionStrategy(OperationStrategy):
    def execute(self, operand1: float, operand2: float) -> float:
        if operand2 == 0:
            raise ValueError("Division by zero is not allowed.")
        return operand1 / operand2
