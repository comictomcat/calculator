from abc import ABC, abstractmethod


class OperationStrategy(ABC):
    @abstractmethod
    def execute(self, operand1: float, operand2: float) -> float:
        pass
