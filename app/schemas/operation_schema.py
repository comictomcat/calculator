from pydantic import BaseModel


class OperationRequest(BaseModel):
    operand1: float
    operand2: float
    operator: str


class OperationResponse(BaseModel):
    result: float
    color: str | None = None
