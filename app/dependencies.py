from app.services.calculator_service import CalculatorService


def get_calculator_service() -> CalculatorService:
    return CalculatorService()
