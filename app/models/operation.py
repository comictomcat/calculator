from pydantic import BaseModel, Field


class Operation(BaseModel):
    operand1: float = Field(..., description="The first operand for the operation")
    operand2: float = Field(..., description="The second operand for the operation")
    operator: str = Field(
        ...,
        description="The operator for the operation, e.g., '+', '-', '*', '/'",
    )
