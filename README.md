
![screenshot](docs/screenshots/main.jpg)

# Calculator API

An extensible web application built with FastAPI for basic arithmetic operations like addition, subtraction, multiplication, and division. Features REST API, dependency injection. Designed for scalability and extensibility, it allows for easy addition of new complex expressions in the future

## Deployment

### Docker (recommended)

```bash
docker-compose up -d  # Run container in a detached (-d) mode
```

Following enviroment variables can be modified if needed:
- HOST_PORT (default: 9000)
- CONTAINER_PORT (default: 9000)

### Locally
```bash
# Install dependencies, virtualenv is recommended
pip install -e .
uvicorn app.main:app --host 0.0.0.0 --port 8000
```

## Running tests

```bash
pytest
```

## Endpoints

#### POST /api/v1/calculate

```bash
curl -X 'POST' \
  'http://0.0.0.0:9000/api/v1/calculate' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "operand1": 5,
  "operand2": 10,
  "operator": "+"
}'
```
```json
{
  "result": 15,
  "color": null
}
```

## Potential improvements
### Serving static files
- Explore alternative methods for serving static files, such as utilizing a separate web server like Nginx or Apache to efficiently deliver static assets.
For simplicity FastAPI's built-in static file serving was used.
