FROM python:3.11-slim-buster 

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set the working directory in the container
WORKDIR /app

# Copy the project files to the working directory
COPY . .

# Install project dependencies
RUN pip install -e .

# Set default values for HOST_PORT, CONTAINER_PORT, and HOST_IP environment variables
ENV CONTAINER_PORT=9000

# Expose the port on which the FastAPI application will run
EXPOSE ${CONTAINER_PORT}

# Command to run the FastAPI application using Uvicorn
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "${CONTAINER_PORT}"]
